# Bug report
<!-- Please make sure you have confirmed this bug has not been reported before -->

Discord Name:

## Description
<!--Describe the bug-->

## Reproduction steps
<!--Write down the steps to reproduce this bug-->
1. Step one
2. Step two

## Media
<!-- Put screenshots or videos here-->
